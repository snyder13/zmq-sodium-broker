#include "common.h"

#include <map>

int main(int, char**) {
	sodiumInit();
	std::cout << "Load static key (both public, server's private):\n";
	// load client's public key
	auto clientPubKey = PublicKey::fromFile("keys/client.pub");
	clientPubKey.hexLog("\tclient public key");

	// load my keys
	auto server = KeyPair::fromFiles("keys/server");
	server.getPublicKey().hexLog("\tserver public key");
	server.getPrivateKey().hexLog("\tserver private key");

	std::cout << "Use Diffie-Hellman to generate a shared secret: (The client can generate the same number using both public keys and its private key)\n";
	// diffie-hellman up a shared key
	auto shared = Hash::serverDiffieHellman(server, clientPubKey);
	shared.hexLog("\tstatic shared");

	// set up interprocess communication (would normally be a socket connection, hence the need for encryption in the first)
	zmq::context_t ctx(1); // param is number of threads to use
	zmq::socket_t logSock(ctx, ZMQ_PULL);
	logSock.bind("ipc://server.zmq");
	zmq::socket_t hsSock(ctx, ZMQ_REP);
	hsSock.bind("ipc://server-hs.zmq");

	zmq::pollitem_t items[] = {
		{ logSock, 0, ZMQ_POLLIN, 0 },
		{ hsSock, 0, ZMQ_POLLIN, 0 }
	};

	// reusable buffers
	unsigned char ciphertext[MAX_MSG_SIZE];
	unsigned char decrypted[MAX_MSG_SIZE];
	sodium_mlock(decrypted, sizeof(decrypted));

	Nonce nonce;
	SessionToken sessionId;

	std::cout << "Wait for message ...\n";
	std::map<SessionToken, Hash> sessions;
	// receive messages
	for (;;) {
		zmq::message_t req;
		zmq::poll(items, 2, -1);

   	if (items[0].revents & ZMQ_POLLIN) {
			std::cout << "Received a message on the log socket\n";
			logSock.recv(&req);

			// verify message is not too large for the reusable buffer or too small to possibly be anything
			if (req.size() > MAX_MSG_SIZE) {
				throw std::runtime_error("message too large");
			}
			if (req.size() < Nonce::size() + SessionToken::size() + crypto_secretbox_MACBYTES + 1) {
				throw std::runtime_error("incomplete message");
			}

			// nonce is concatenated with the rest of the message, take that many bytes to get it
			nonce.reset(static_cast<unsigned char*>(req.data()));
			nonce.hexLog("\tnonce");

			// next field is the session id
			sessionId.reset(static_cast<unsigned char*>(req.data()) + nonce.size());
			sessionId.hexLog("\tsession");

			hexLog("\tciphertext", static_cast<unsigned char*>(req.data()) + nonce.size() + sessionId.size(), req.size() - nonce.size() - sessionId.size());

			std::cout << "\tMatch session with map of prior negotiations to find the shared secret that can decode:\n";
			auto it = sessions.find(sessionId);
			if (it == sessions.end()) {
				throw std::runtime_error("invalid session");
			}
			it->second.hexLog("\t\tsecret");

			// read the rest of the message as the payload
			size_t ciphertextLen = req.size() - nonce.size() - sessionId.size();

			// decrypt
			if (crypto_secretbox_open_easy(decrypted, static_cast<const unsigned char*>(req.data()) + nonce.size() + sessionId.size(), ciphertextLen, nonce, it->second) != 0) {
				throw std::runtime_error("forged message");
			}
			decrypted[ciphertextLen - crypto_secretbox_MACBYTES] = '\0';
			std::cout << "\t\tplaintext: [" << decrypted << "]\n";
		}
		if (items[1].revents & ZMQ_POLLIN) {
			hsSock.recv(&req);
			std::cout << "Got a message on the key exchange socket\n";
			if (req.size() != Nonce::size() + PublicKey::size() + crypto_secretbox_MACBYTES) {
				throw std::runtime_error("bad public key size");
			}

			// nonce is concatenated with the rest of the message, take that many bytes to get it
			nonce.reset(static_cast<unsigned char*>(req.data()));
			nonce.hexLog("\tnonce");
			hexLog("\tciphertext", static_cast<unsigned char*>(req.data()) + nonce.size(), req.size() - nonce.size());

			// read payload from rest of message
			size_t ciphertextLen = req.size() - crypto_secretbox_NONCEBYTES;
			memcpy(ciphertext, req.data() + crypto_secretbox_NONCEBYTES, ciphertextLen);

			std::cout << "\tUse the static secret to decrypt a new public key the client is suggesting to use for the session:\n";
			// decrypt
			if (crypto_secretbox_open_easy(decrypted, ciphertext, ciphertextLen, nonce, shared) != 0) {
				throw std::runtime_error("forged message");
			}
			hexLog("\t\tclient session pub", decrypted, crypto_box_PUBLICKEYBYTES);

			std::cout << "\tGenerate our own pair of keys to use for the session:\n";
			// generate a pair of ephemeral keys
			auto session = KeyPair::random();
			session.getPublicKey().hexLog("\t\tserver session public key");
			session.getPrivateKey().hexLog("\t\tserver session private key");

			std::cout << "\tUse Diffie-Hellman on the session keys to get a temporary secret: Again, the client can get the same number by using its pair of session public keys along with its private key\n";
			// generate shared secret for the session
			auto sessShared = Hash::serverDiffieHellman(session, PublicKey::fromBytes(decrypted));
			hexLog("\t\tsession shared", sessShared, PublicKey::size());


			// done with session private key
			session.getPrivateKey().destroy();
			std::cout << "\t\tzeroed memory of the session private key\n";

			std::cout << "\tGenerate a public token that we can use to link future traffic back to this negotiation:\n";
			// generate a session token
			SessionToken tok;
			tok.hexLog("\t\tsession token");

			std::cout << "\tSend back a message with the token and the public portion of our session keys:\n";
			// encrypt public key, session token
			auto sessionReplyCipher = shared.encrypt(session.getPublicKey());
			sessionReplyCipher.getNonce().hexLog("\t\toutgoing nonce");
			sessionReplyCipher.hexLog("\t\tciphertext");

			// send the public key
			zmq::message_t hs(nonce.size() + tok.size() + sessionReplyCipher.size());
			memcpy(hs.data(), sessionReplyCipher.getNonce(), Nonce::size());
			memcpy(static_cast<char*>(hs.data()) + nonce.size(), tok, tok.size());
			memcpy(static_cast<char*>(hs.data()) + nonce.size() + tok.size(), sessionReplyCipher, sessionReplyCipher.size());
			hsSock.send(hs);

			// store session secret by session id
			sessions.insert(std::make_pair(tok, sessShared));
		}
	}
}

#include <iostream>
#include <fstream>

#include <sodium.h>

auto main(int argc, char** argv) -> int {
	unsigned char pubKey[crypto_box_PUBLICKEYBYTES] = {};
	unsigned char privKey[crypto_box_SECRETKEYBYTES] = {};

	if (argc != 2) {
		std::cerr << "Usage: keygen <outfile>\n\tWill write a key pair in <outfile> and <outfile>.pub\n";
		return 1;
	}

	std::fstream privFile(argv[1], std::ios_base::out|std::ios_base::trunc),
		pubFile((std::string(argv[1]) + ".pub").c_str(), std::ios_base::out|std::ios_base::trunc);

	if (sodium_init() == -1) {
		std::cerr << "failed to initialize libsodium\n";
		return 1;
	}

	crypto_box_keypair(pubKey, privKey);

	char pubHex[crypto_box_PUBLICKEYBYTES*2 + 1];
	sodium_bin2hex(pubHex, crypto_box_PUBLICKEYBYTES*2 + 1, pubKey, crypto_box_PUBLICKEYBYTES);
	char privHex[crypto_box_SECRETKEYBYTES*2 + 1];
	sodium_bin2hex(privHex, crypto_box_SECRETKEYBYTES*2 + 1, privKey, crypto_box_SECRETKEYBYTES);

	pubFile << pubHex;
	privFile << privHex;

	return 0;
}

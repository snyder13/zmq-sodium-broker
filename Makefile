CXX ?= g++
CXXFLAGS = -Wall -Wextra -Weffc++ -std=c++11 -O3 -pedantic
LIBS = -lzmq -lsodium

all: keys server client

server: server.cc common.h
	${CXX} ${CXXFLAGS} server.cc -o server ${LIBS}

client: client.cc common.h
	${CXX} ${CXXFLAGS} client.cc -o client ${LIBS}

keys: keygen
	-mkdir keys
	./keygen keys/server
	./keygen keys/client

keygen: keygen.cc
	${CXX} ${CXXFLAGS} keygen.cc -o keygen ${LIBS}


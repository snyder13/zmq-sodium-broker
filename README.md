# zmq-sodium-broker

## Motivation
To create a daemon that will accept "regular" ZeroMQ messages from a variety of clients and forward them securely to a logging server.

Securely meaning using libsodium's elliptical curve cryptography and ephemeral key exchange to attain forward secrecy.

Having a broker do key exchange has a few benefits:

  * Client libraries are simpler than they would be if required to implement their own libsodium wrappers
  * Clients do not have to complete negotiation before they can send messages, which would at best make applications slower and at worst crash them, if they don't respond well to intermittent network errors and the like.
  * It is much more efficient in cases of short-running applications like PHP page loads to not have to renegotiate every time they are invoked.

And a few drawbacks:

  * The broker must take responsibility for messages it receives when it cannot forward them immediately (queueing and/or flushing to its own log)
  * The broker must be supervised to prevent message losses if it crashes, is killed, deadlocks, etc

## State
Currently, it's just a proof of concept to show how libsodium can work on top of ZeroMQ.

### TODO
 * Forward to real logging server
 * Address drawbacks in "motivation" section:
    * Fallback log
	 * Supervisor
 * Session keys expiration mechanism

## Building
 * Install prerequisites: `build-essential libzmq-dev libsodium-dev`
 * Run `make` to build the example client and server and to generate keys for each

## Running
 * Run the `server` program and either send it to the background or open a new terminal session for the next step
 * Run `client`

## Example output

### Server

    Load static key (both public, server's private):
            client public key: [ce66083bd77ce2161086febd638b0b70f2559f42928da96a91ca938af7866b05]
            server public key: [73ac71043ba050be22e91699a0407f7a3d92ee93557a53927f49975f49f7536b]
            server private key: [3e63c7ab183bc0925ee07623f8ec07a7eca9754853d322dc6aef7c3de6b36a73]
    Use Diffie-Hellman to generate a shared secret: (The client can generate the same number using both public keys and its private key)
            static shared: [f1823b95ec3d53524302f06ad1bb9eff2986a8962f837a50c7389a08b00672e3]
    Wait for message ...
    Got a message on the key exchange socket
            nonce: [b9c0ed15e3a7026d2190732b931872155caf3abcdda3894d]
            ciphertext: [be9bec0b4907520570f9d62189d20e7d709a9674aa098a3353bd9fd7bd5d289d1a97454757306a6229209ef2e5050261]
            Use the static secret to decrypt a new public key the client is suggesting to use for the session:
                    client session pub: [b97796f49ffa70ccd3a0ac1a2a6a0aa162cf40038b6a6b24921e27624caca77a]
            Generate our own pair of keys to use for the session:
                    server session public key: [ddc63c4eefde2b498f5de2866b3e142f124f28d6f10e5bd6ad044e2ff6122214]
                    server session private key: [50163b0dfb57be9a5fea2449b9abd4bd57fbc7263ccab9f5d13e7308271f6d3e]
            Use Diffie-Hellman on the session keys to get a temporary secret: Again, the client can get the same number by using its pair of session public keys along with its private key
                    session shared: [eb9244c58f0bf8c33319b1fcc23a3f2731bb70d8ef38a6c4a0079df7247652e3]
                    zeroed memory of the session private key
            Generate a public token that we can use to link future traffic back to this negotiation:
                    session token: [6f207eab37154e85458d14989bc0c776798b60db62573514]
            Send back a message with the token and the public portion of our session keys:
                    outgoing nonce: [df43f2870a89a5cbcfc03b1472f36872afc9b40a5c3d99ee]
                    ciphertext: [327354a71ef4a7f2068b0466a1ae28a9d1f4fd7ad5efd8b61af861a2c207266b2bd5f0b19d2a338110cdb0c38cda73e4]
    Received a message on the log socket
            nonce: [56703e811859124004eba26acc8096031402c03986f55e41]
            session: [6f207eab37154e85458d14989bc0c776798b60db62573514]
            ciphertext: [6dbdf8c27d48be8ff18fbd0180f957889b4ab998193e32]
            Match session with map of prior negotiations to find the shared secret that can decode:
                    secret: [eb9244c58f0bf8c33319b1fcc23a3f2731bb70d8ef38a6c4a0079df7247652e3]
                    plaintext: [hello 1]
    Received a message on the log socket
            nonce: [120ad9f19f60e1ebc079a32beb9fc92e44f4153b051c9088]
            session: [6f207eab37154e85458d14989bc0c776798b60db62573514]
            ciphertext: [5c116875041a45e40e17e2c2356a942626e907f857152e]
            Match session with map of prior negotiations to find the shared secret that can decode:
                    secret: [eb9244c58f0bf8c33319b1fcc23a3f2731bb70d8ef38a6c4a0079df7247652e3]
                    plaintext: [hello 2]
    Received a message on the log socket
            nonce: [154363f6bcdfbccbd1cc43b2f4130842ca85d0f93f373d17]
            session: [6f207eab37154e85458d14989bc0c776798b60db62573514]
            ciphertext: [8693ea10c964f42b16ac9c3687161c145259e6b43ff17f]
            Match session with map of prior negotiations to find the shared secret that can decode:
                    secret: [eb9244c58f0bf8c33319b1fcc23a3f2731bb70d8ef38a6c4a0079df7247652e3]
                    plaintext: [hello 3]
### Client
    Load static key (both public, client's private):
            server public key: [73ac71043ba050be22e91699a0407f7a3d92ee93557a53927f49975f49f7536b]
            client private key: [bad1851866ade8fa77c33f02fead97a286c7ef8cba9d97063573b6cda5481434]
            client public key: [ce66083bd77ce2161086febd638b0b70f2559f42928da96a91ca938af7866b05]
    Use Diffie-Hellman to generate a shared secret: (The server can generate the same number using both public keys and its private key)
            static shared: [f1823b95ec3d53524302f06ad1bb9eff2986a8962f837a50c7389a08b00672e3]
            zeroed memory of the private key
    Generate a pair of ephemeral keys to use only for the session:
            client session private key: [3f3ca2129da52085c48c34dd9ab2267bf5c9ed7c603c554391a3a44fa2ccc884]
            client session public key: [b97796f49ffa70ccd3a0ac1a2a6a0aa162cf40038b6a6b24921e27624caca77a]
    Encrypt the public key with the static secret to send to the server:
            nonce: [b9c0ed15e3a7026d2190732b931872155caf3abcdda3894d]
            ciphertext: [be9bec0b4907520570f9d62189d20e7d709a9674aa098a3353bd9fd7bd5d289d1a97454757306a6229209ef2e5050261]
    Got response:
            nonce: [df43f2870a89a5cbcfc03b1472f36872afc9b40a5c3d99ee]
            session id: [6f207eab37154e85458d14989bc0c776798b60db62573514]
            ciphertext: [327354a71ef4a7f2068b0466a1ae28a9d1f4fd7ad5efd8b61af861a2c207266b2bd5f0b19d2a338110cdb0c38cda73e4]
    Decrypt with static secret:
            server session public key: [ddc63c4eefde2b498f5de2866b3e142f124f28d6f10e5bd6ad044e2ff6122214]
            zeroed static secret
            Use Diffie-Hellman on the session keys to get a temporary secret: Again, the server can get the same number by using its pair of session public keys along with its private key
    session shared: [eb9244c58f0bf8c33319b1fcc23a3f2731bb70d8ef38a6c4a0079df7247652e3]
            zeroed session private key
    Send some messages on the session
            Sending: hello 1
                    nonce: [56703e811859124004eba26acc8096031402c03986f55e41]
                    session: [6f207eab37154e85458d14989bc0c776798b60db62573514]
                    ciphertext: [6dbdf8c27d48be8ff18fbd0180f957889b4ab998193e32]
            Sending: hello 2
                    nonce: [120ad9f19f60e1ebc079a32beb9fc92e44f4153b051c9088]
                    session: [6f207eab37154e85458d14989bc0c776798b60db62573514]
                    ciphertext: [5c116875041a45e40e17e2c2356a942626e907f857152e]
            Sending: hello 3
                    nonce: [154363f6bcdfbccbd1cc43b2f4130842ca85d0f93f373d17]
                    session: [6f207eab37154e85458d14989bc0c776798b60db62573514]
                    ciphertext: [8693ea10c964f42b16ac9c3687161c145259e6b43ff17f]

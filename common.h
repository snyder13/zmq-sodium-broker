#ifndef ZMQ_SODIUM_DH_COMMON_H
#define ZMQ_SODIUM_DH_COMMON_H

#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include <sys/resource.h> // setrlimit

#include <sodium.h>
#include <zmq.hpp>

const auto MAX_MSG_SIZE = 2000000u;

class PublicKey;
class KeyPair;
class Hash;
class Nonce;

template <size_t payloadLen, size_t macLen>
class EncryptedMessage;
template <size_t payloadLen>
class DecryptedMessage;
template <size_t macLen>
class DynamicEncryptedMessage;
class SessionToken;


template <size_t len>
class Bytes
{
	template <size_t anyLen>
	friend class Bytes;

	friend class std::less<SessionToken>;
public:
	operator unsigned char*() {
		return bytes;
	}

	operator const unsigned char*() const {
		return bytes;
	}

	static constexpr auto size() -> size_t {
		return len;
	}

	// zero memory of private keys
	~Bytes() {
		destroy();
	}
	auto destroy() -> void {
		if (locked) {
			sodium_memzero(bytes, len);
			sodium_munlock(bytes, len);
			locked = false;
		}
	}

	template <size_t otherLen>
	auto operator+(Bytes<otherLen> other) -> Bytes<len + otherLen> {
		Bytes<len + otherLen> rv(locked || other.locked, false);
		memcpy(rv.bytes, bytes, len);
		memcpy(rv.bytes + len, other.bytes, otherLen);
		destroy();
		other.destroy();
		return rv;
	}

	auto toHex() const -> std::string {
		char hex[len*2 + 1];
		hex[len*2] = '\0';
		sodium_bin2hex(hex, len*2 + 1, bytes, len);
		return hex;
	}

	auto hexLog(const std::string& key = "") const -> void {
		if (key != "") {
			std::cout << key << ": ";
		}
		std::cout << "[" << toHex() << "]\n";
	}

	static auto serverDiffieHellman(const KeyPair& pair, const PublicKey& otherPub) -> Hash;
	static auto clientDiffieHellman(const KeyPair& pair, const PublicKey& otherPub) -> Hash;

	auto reset(const unsigned char data[len] = nullptr) -> void {
		if (data == nullptr) {
			randombytes_buf(bytes, len);
		}
		else {
			memcpy(bytes, data, len);
		}
	}

	template <size_t msgLen>
	auto encrypt(const Bytes<msgLen>& data) -> EncryptedMessage<msgLen, crypto_secretbox_MACBYTES>;
	auto encrypt(const std::string& data) -> DynamicEncryptedMessage<crypto_secretbox_MACBYTES>;

	template <size_t msgLen>
	auto sessionDecrypt(const unsigned char* const data, size_t actualSize) -> DecryptedMessage<msgLen>;

	Bytes(bool lock, bool initialize = true) : locked(lock) {
		// protect memory of private keys
		if (lock) {
			sodium_mlock(bytes, len);
		}
		if (initialize) {
			reset();
		}
	}

	Bytes(const std::string& path, bool lock) : locked(lock) {
		// protect memory of private keys
		if (lock) {
			sodium_mlock(bytes, len);
		}
		std::fstream fp(path, std::ios_base::in);
		std::string hex;
		fp >> hex;
		if (hex.length() != len*2) {
			throw std::runtime_error("unexpected key size in " + path);
		}
		size_t binLen;
		const char* _hexEnd;
		sodium_hex2bin(bytes, len, hex.c_str(), hex.length(), NULL, &binLen, &_hexEnd);
		if (binLen != len) {
			throw std::runtime_error("failed to decode hex in " + path);
		}
	}

	Bytes(const unsigned char* const buf, bool lock) {
		if (lock) {
			sodium_mlock(bytes, len);
		}
		memcpy(bytes, buf, len);
	}
private:
	static auto diffieHellman(const KeyPair& pair, const PublicKey& otherPub, const bool isServer) -> Hash;

	bool locked;
	unsigned char bytes[len];
};

class Nonce : public Bytes<crypto_secretbox_NONCEBYTES>
{
public:
	Nonce(bool initialize = true) : Bytes(false, initialize) {
	}
};

class SessionToken : public Bytes<crypto_secretbox_NONCEBYTES>
{
public:
	SessionToken(bool initialize = true) : Bytes(false, initialize) {
	}
	static auto fromBytes(const unsigned char data[crypto_secretbox_NONCEBYTES]) -> SessionToken {
		auto rv = SessionToken(false);
		rv.reset(data);
		return rv;
	}
};

namespace std
{
	template<>
	struct less<SessionToken>
	{
		bool operator() (const SessionToken& lhs, const SessionToken& rhs) const {
			return memcmp(lhs.bytes, rhs.bytes, SessionToken::size()) < 0;
		}
	};
}

template <size_t payloadLen, size_t macLen>
class EncryptedMessage : public Bytes<payloadLen + macLen>
{
public:
	EncryptedMessage() : Bytes<payloadLen + macLen>(true, false), nonce() {
	}
	auto getNonce() const -> const Nonce& {
		return nonce;
	}
	static constexpr auto payloadSize() -> size_t { return payloadLen; }
	static constexpr auto size() -> size_t { return payloadLen + macLen; }
	static constexpr auto getMacSize() -> size_t { return macLen; }
private:
	Nonce nonce;
};

template <size_t payloadLen>
class DecryptedMessage : public Bytes<payloadLen>
{
public:
	template <typename T>
	auto as() -> T {
		T rv = *this;
		return rv;
	}

	DecryptedMessage(const unsigned char* const data) : Bytes<payloadLen>(true, false), nonce(false), tok(false) {
		nonce.reset(data);
		tok.reset(data + nonce.size());
		this->reset(data + nonce.size() + tok.size());
	}
	auto getNonce() const -> const Nonce& {
		return nonce;
	}
	auto getSessionToken() const -> const SessionToken& {
		return tok;
	}
	static constexpr auto size() -> size_t { return payloadLen; }
private:
	Nonce nonce;
	SessionToken tok;
};

class Hash : public Bytes<crypto_generichash_BYTES>
{
	friend class Bytes;
private:
	Hash(bool lock, bool initialize) : Bytes(lock, initialize) {
	}
};

class PublicKey : public Bytes<crypto_box_PUBLICKEYBYTES>
{
public:
	static auto random() -> PublicKey {
		return PublicKey();
	}
	static auto fromBytes(const unsigned char* const bytes) -> PublicKey {
		auto rv = PublicKey(false);
		rv.reset(bytes);
		return rv;
	}
	static auto fromBytes(const void* const bytes) -> PublicKey {
		return fromBytes(static_cast<const unsigned char*>(bytes));
	}
	static auto fromFile(const std::string& path) -> PublicKey {
		return PublicKey(path);
	}
	static auto empty() -> PublicKey {
		return PublicKey(false);
	}
private:
	PublicKey(bool initialize = true) : Bytes(false, initialize) {
	}
	PublicKey(const std::string& path) : Bytes(path, false) {
	}
};

class PrivateKey : public Bytes<crypto_box_SECRETKEYBYTES>
{
public:
	static auto random() -> PrivateKey {
		return PrivateKey();
	}
	static auto fromFile(const std::string& path) -> PrivateKey {
		return PrivateKey(path);
	}
	static auto empty() -> PrivateKey {
		return PrivateKey(false);
	}
private:
	PrivateKey(bool initialize = true) : Bytes(true, initialize) {
	}
	PrivateKey(const std::string& path) : Bytes(path, true) {
	}
};

class KeyPair
{
public:
	static auto fromFiles(const std::string& basePath) -> KeyPair {
		return KeyPair(basePath + ".pub", basePath);
	}
	static auto random() -> KeyPair {
		return KeyPair();
	}
	auto getPublicKey() -> PublicKey& {
		return pub;
	}
	auto getPrivateKey() -> PrivateKey& {
		return priv;
	}
	auto getPublicKey() const -> const PublicKey& {
		return pub;
	}
	auto getPrivateKey() const -> const PrivateKey& {
		return priv;
	}
private:
	KeyPair() : pub(PublicKey::empty()), priv(PrivateKey::empty()) {
		crypto_box_keypair(pub, priv);
	}
	KeyPair(const std::string& pubPath, const std::string& privPath) : pub(PublicKey::fromFile(pubPath)), priv(PrivateKey::fromFile(privPath)) {
	}
	PublicKey pub;
	PrivateKey priv;
};

template <size_t len>
auto Bytes<len>::diffieHellman(const KeyPair& keys, const PublicKey& otherPub, const bool isServer) -> Hash {
	unsigned char q[crypto_scalarmult_BYTES] = {};

	// derive shared secret from my private and the other end's public key
	if (crypto_scalarmult(q, keys.getPrivateKey(), otherPub) != 0) {
		throw std::runtime_error("failed scalar multiplication");
	}

	// combine secret and public keys into shared key
	crypto_generichash_state h;
	crypto_generichash_init(&h, NULL, 0U, crypto_generichash_BYTES);
	crypto_generichash_update(&h, q, sizeof(q));

	// can provide a more consistent API by switching the order of these
	// fields around here depending on which end of the communication the
	// caller is on
	if (isServer) {
		crypto_generichash_update(&h, otherPub, crypto_box_PUBLICKEYBYTES);
		crypto_generichash_update(&h, keys.getPublicKey(), crypto_box_PUBLICKEYBYTES);
	}
	else {
		crypto_generichash_update(&h, keys.getPublicKey(), crypto_box_PUBLICKEYBYTES);
		crypto_generichash_update(&h, otherPub, crypto_box_PUBLICKEYBYTES);
	}
	Hash shared(true, false);
	crypto_generichash_final(&h, shared, crypto_generichash_BYTES);
	return shared;
}

template <size_t len>
auto Bytes<len>::serverDiffieHellman(const KeyPair& pair, const PublicKey& otherPub) -> Hash {
	return diffieHellman(pair, otherPub, true);
}
template <size_t len>
auto Bytes<len>::clientDiffieHellman(const KeyPair& pair, const PublicKey& otherPub) -> Hash {
	return diffieHellman(pair, otherPub, false);
}

template <size_t len>
template <size_t msgLen>
auto Bytes<len>::encrypt(const Bytes<msgLen>& data) -> EncryptedMessage<msgLen, crypto_secretbox_MACBYTES> {
	EncryptedMessage<msgLen, crypto_secretbox_MACBYTES> rv;
	crypto_secretbox_easy(rv, data, msgLen, rv.getNonce(), bytes);
	return rv;
}

template <size_t macLen>
class DynamicEncryptedMessage
{
public:
	DynamicEncryptedMessage(size_t size) : payloadLen(size), buf(size + macLen), nonce() {
	}
	operator unsigned char*() {
		return &buf[0];
	}
	auto getNonce() const -> const Nonce& {
		return nonce;
	}
	auto payloadSize() -> size_t { return payloadLen; }
	auto size() -> size_t { return payloadLen + macLen; }
	static constexpr auto getMacSize() -> size_t { return macLen; }
private:
	size_t payloadLen;
	std::vector<unsigned char> buf;
	Nonce nonce;
};

template <size_t len>
auto Bytes<len>::encrypt(const std::string& data) -> DynamicEncryptedMessage<crypto_secretbox_MACBYTES> {
	if (data.length() + Nonce::size() + SessionToken::size() + crypto_secretbox_MACBYTES >= MAX_MSG_SIZE) {
		throw std::runtime_error("message too large");
	}
	DynamicEncryptedMessage<crypto_secretbox_MACBYTES> rv(data.size());
	crypto_secretbox_easy(rv, reinterpret_cast<const unsigned char*>(data.c_str()), data.length(), rv.getNonce(), bytes);
	return rv;
}

/**
 * hex encode a bit of binary for debugging
 */
auto toHex(const unsigned char* const data, const size_t len) -> std::string {
	auto hex = new char[len*2 + 2]();
	hex[len*2 + 1] = '\0';
	sodium_bin2hex(hex, len*2 + 1, data, len);
	std::string rv(hex);
	delete[] hex;
	return rv;
}

template <size_t len>
template <size_t payloadLen>
auto Bytes<len>::sessionDecrypt(const unsigned char* data, size_t actualSize) -> DecryptedMessage<payloadLen> {
	if (actualSize != Nonce::size() + SessionToken::size() + payloadLen + crypto_secretbox_MACBYTES) {
		throw std::runtime_error("unexpected session pack size");
	}
	DecryptedMessage<payloadLen> rv(data);
	if (crypto_secretbox_open_easy(rv, data + Nonce::size() + SessionToken::size(), payloadLen + crypto_secretbox_MACBYTES, rv.getNonce(), bytes) != 0) {
		throw std::runtime_error("forged message");
	}
	return rv;
}

/**
 * initialize libsodium
 */
auto sodiumInit() -> void {
	static bool first = true;
	if (first) {
		first = false;
		// disable core dumps when used in production to avoid leaking keys
#ifndef DEBUG
		struct rlimit noCores = {0, 0};
		setrlimit(RLIMIT_CORE, &noCores);
#endif
		if (sodium_init() == -1) {
			throw std::runtime_error("failed to initialize libsodium\n");
		}
	}
}

/**
 * log hex of a bit of binary for debugging
 */
auto hexLog(const std::string lbl, const unsigned char* const data, const size_t len) -> void {
	std::cout << lbl << ": [" << toHex(data, len) << "]\n";
}
#endif

#include "common.h"

int main(int, char**) {
	sodiumInit();

	std::cout << "Load static key (both public, client's private):\n";
	// load server's public key
	auto serverPubKey = PublicKey::fromFile("keys/server.pub");
	serverPubKey.hexLog("\tserver public key");

	// load my keys
	auto client = KeyPair::fromFiles("keys/client");
	client.getPrivateKey().hexLog("\tclient private key");
	client.getPublicKey().hexLog("\tclient public key");

	std::cout << "Use Diffie-Hellman to generate a shared secret: (The server can generate the same number using both public keys and its private key)\n";
	// diffie-hellman up a symmetric key
	auto shared = Hash::clientDiffieHellman(client, serverPubKey);
	shared.hexLog("\tstatic shared");

	// zero memory for private key, no longer needed
	client.getPrivateKey().destroy();
	std::cout << "\tzeroed memory of the private key\n";

	std::cout << "Generate a pair of ephemeral keys to use only for the session:\n";
	// create some ephemeral keys
	auto session = KeyPair::random();
	session.getPrivateKey().hexLog("\tclient session private key");
	session.getPublicKey().hexLog("\tclient session public key");

	// set up interprocess communication (would normally be a socket connection, hence the need for encryption in the first)
	zmq::context_t ctx(1); // param is # of threads to use
	// "normal" message channel
	zmq::socket_t logSock(ctx, ZMQ_PUSH);
	logSock.connect("ipc://server.zmq");
	// preliminary channel to agree on a session key for forward secrecy
	zmq::socket_t hsSock(ctx, ZMQ_REQ);
	hsSock.connect("ipc://server-hs.zmq");

	std::cout << "Encrypt the public key with the static secret to send to the server:\n";
	// encode the session public key with the static key
	auto encSessKey = shared.encrypt(session.getPublicKey());
	encSessKey.getNonce().hexLog("\tnonce");
	encSessKey.hexLog("\tciphertext");

	// send public key to server
	const size_t sessKeyPayloadSize = PublicKey::size() + encSessKey.getMacSize();
	zmq::message_t hs(Nonce::size() + sessKeyPayloadSize);
	// nonce
	memcpy(hs.data(), encSessKey.getNonce(), Nonce::size());
	// skip nonce area, then public key
	memcpy(static_cast<char*>(hs.data()) + Nonce::size(), encSessKey, sessKeyPayloadSize);
	hsSock.send(hs);

	zmq::message_t resp;
	hsSock.recv(&resp);
	std::cout << "Got response:\n";
	hexLog("\tnonce", static_cast<unsigned char*>(resp.data()), Nonce::size());
	hexLog("\tsession id", static_cast<unsigned char*>(resp.data()) + Nonce::size(), SessionToken::size());
	hexLog("\tciphertext", static_cast<unsigned char*>(resp.data()) + Nonce::size() + SessionToken::size(), resp.size() - Nonce::size() - SessionToken::size());

	std::cout << "Decrypt with static secret:\n";
	// receive the ephemeral public key from the server's side and a session token to carry on
	auto serverSessPub = shared.sessionDecrypt<PublicKey::size()>(static_cast<unsigned char*>(resp.data()), resp.size());
	auto sessionId = serverSessPub.getSessionToken();
	auto serverSessPubKey = PublicKey::fromBytes(serverSessPub);
	serverSessPubKey.hexLog("\tserver session public key");

	// using ephemeral keys from here out, zero shared secret derived from static keys
	shared.destroy();
	std::cout << "\tzeroed static secret\n";

	std::cout << "\tUse Diffie-Hellman on the session keys to get a temporary secret: Again, the server can get the same number by using its pair of session public keys along with its private key\n";
	// use the server's public key with our ephemeral pair to get a shared secret just for this session
	auto sessShared = Hash::clientDiffieHellman(session, serverSessPubKey);
	// zero the memory for the ephemeral private key, no longer needed
	session.getPrivateKey().destroy();

	hexLog("session shared", sessShared, sessShared.size());
	std::cout << "\tzeroed session private key\n";

	std::cout << "Send some messages on the session\n";
	// send the damn message already
	for (size_t idx = 1; idx <= 3; ++idx) {
		const std::string message = "hello " + std::to_string(idx);
		std::cout << "\tSending: " << message << "\n";

		auto encrypted = sessShared.encrypt(message);

		hexLog("\t\tnonce", encrypted.getNonce(), Nonce::size());
		hexLog("\t\tsession", sessionId, SessionToken::size());
		hexLog("\t\tciphertext", encrypted, encrypted.size());

		// concatenate nonce, session token, and payload and send down the wire
		zmq::message_t logMsg(Nonce::size() + SessionToken::size() + encrypted.size());
		memcpy(logMsg.data(), encrypted.getNonce(), Nonce::size());
		memcpy(static_cast<unsigned char*>(logMsg.data()) + Nonce::size(), sessionId, SessionToken::size());
		memcpy(static_cast<unsigned char*>(logMsg.data()) + Nonce::size() + SessionToken::size(), encrypted, encrypted.size());
		logSock.send(logMsg);
	}
}
